import { Link } from "react-router-dom";
import { Menu } from "antd";
import { useSelector, useDispatch } from "react-redux"
import { useLocation } from "react-router";
import { logout } from "../stores/auth-slice";
import { UserOutlined } from '@ant-design/icons';





export function Navigateur() {

    const location = useLocation();

    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

    return (
        <Menu mode="horizontal" selectedKeys={[location.pathname]} style={{ background: '#a0d911' }}>
            <Menu.Item key="/">
                <Link to="/"style={{ fontSize: "20px" }}>Home</Link>
            </Menu.Item>

            {!user ? <>
                <Menu.Item key="/register">
                    <Link to="/register" style={{ fontSize: "25px" }}>S'inscrire</Link>
                </Menu.Item>

                <Menu.Item key="/login">
                    <Link to="/login" ><UserOutlined style={{ fontSize: "25px" }} /></Link>
                </Menu.Item>
            </>

                :
                <><Menu.Item key="/Poster" style={{ fontSize: "25px" }}>
                    <Link to="/poster">Poster</Link>
                </Menu.Item>

                    <Menu.Item key="/logout" style={{ fontSize: "25px"}} onClick={() => dispatch(logout())}>
                        <Link to="/logout">Déconnecter</Link>
                    </Menu.Item>

                </>}



        </Menu>
    )
}