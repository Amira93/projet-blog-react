import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../stores/auth-slice';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';


export function UserFormulaire() {

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.registerFeedback);

    let history = useHistory();
    function handleClick() {
        history.push("/");
      }   
    const onFinish = (values) => {
        dispatch(register(values));
        handleClick();
    }

    return (
        <Form 
        // style={{border: '1px solid black', borderRadius: "30px" ,background: 'rgb(184, 184, 184)', marginTop: "70px", paddingTop: "30px"}}
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 7,
            }}
            onFinish={onFinish}

        >

            {feedback && <p>{feedback}</p>}
            <Form.Item 
                name='name'
                label="Name"
                rules={[{ required: true, message: 'Please input your Username!' }]}
            >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>

            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input
                    prefix={<MailOutlined className="site-form-item-icon" />}
                    type="email"
                    placeholder="Email"
                />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your Password!' }]}
            >
                <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                S'inscrire
                </Button>
            </Form.Item>
        </Form>
    )
}