import axios from "axios";



export class AuthService {


    static async register(user){
        const response = await axios.post(' https://projet-blog-api.herokuapp.com/api/user', user);

        return response.data;
    }

    static async login(credentials){
        const response = await axios.post(' https://projet-blog-api.herokuapp.com/api/user/login', credentials);

        localStorage.setItem('token', response.data.token);

        return response.data.user;
    }

    static async fetchAcount(){
        const response = await axios.get(' https://projet-blog-api.herokuapp.com/api/user/account');

        return response.data ;
    }

    static async addArticle(article){
         await axios.post(' https://projet-blog-api.herokuapp.com/api/article', article);
    }

    
}