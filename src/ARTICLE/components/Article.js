import { Button, Card, Col } from "antd";
import Meta from "antd/lib/card/Meta";
import { useDispatch } from "react-redux";
import { DeleteOutlined, UserOutlined } from '@ant-design/icons';
import { Link} from "react-router-dom";
import { deleteArticles, fetchArticle } from "../app/articleSlice";
import Avatar from "antd/lib/avatar/avatar";


export function Article({ article }) {
    const dispatch = useDispatch()
    const link = "/article/" + article.id;

    const onDelete = (id) => {
        dispatch(deleteArticles(id))
        dispatch(fetchArticle())
    }

    // useEffect(() => {
    //     dispatch(fetchArticle());
    // }, [dispatch])



    return (
        <Col>
            <Card
                hoverable
                // style={{ height: 100, width: 900, marginTop: 30 }}
                cover={<img alt={article.titre} src={article.image} style={{ height: 300 }} />}
                extra={<Link to={link}>More</Link>}
                actions={[
                    <Button onClick={() => onDelete(article.id)}  type="default" htmlType="submit"><DeleteOutlined /></Button>

                ]}
            >
                <Meta title={article.titre} description={article.contenu} /><br></br>
                {<Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined  />} />}
                  Publier par: {article.user.name}

            </Card>
        </Col>

    )
}
