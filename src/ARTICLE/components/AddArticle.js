import { Form, Input, Button} from 'antd';
import { useDispatch } from 'react-redux';
import { UserOutlined} from '@ant-design/icons';
import { Addarticle } from '../app/articleSlice';
import { useHistory } from 'react-router-dom';


export function AddArticle() {

    const dispatch = useDispatch();
    let history = useHistory();
    function handleClick() {
        history.push("/");
      }   
    const onFinish = (values) => {
        dispatch(Addarticle(values));
        handleClick()
    }
    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 7,
            }}
            onFinish={onFinish}

        >

            <Form.Item
                name='titre'
                label="Titre"
                rules={[{ required: true, message: 'Please input your Title!' }]}
            >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Titre" />
            </Form.Item>

            <Form.Item
                label="Contenu"
                name="contenu"
                rules={[{ required: true, message: 'Please input your Contenu!' }]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Contenu" />
            </Form.Item>

           
            <Form.Item 
                label="image"
                name="image"
                rules={[{ required: true, message: 'Please input your Image!' }]}
                >
                    <Input className="site-form-item-icon" placeholder="Image" />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Poster
                </Button>
            </Form.Item>
        </Form>
    )
}