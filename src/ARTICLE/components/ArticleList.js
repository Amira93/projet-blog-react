import { Col, Row } from "antd";
import { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchArticle } from "../app/articleSlice";
import { Article } from "./Article";


export function ArticleList() {

    const dispatch = useDispatch()
    const articles = useSelector(state => state.articles.list);

     
    useEffect(() => {
        dispatch(fetchArticle());
    }, [dispatch]);

    return (
        <Row gutter={[30,30]}>
            {articles.map(item =>
                <Col span= {12}key={item.id} >
                    <Article article={item} />
                </Col>
            )}

        </Row>
    );
}