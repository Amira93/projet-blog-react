import { Space } from "antd";
import Search from "antd/lib/transfer/search";
import { useDispatch } from "react-redux";
import { Searcharticle } from "../app/articleSlice";


export function SearchArticle() {

  const dispatch = useDispatch();
  const onSearch = () => {
    dispatch(Searcharticle())

  }

  return (
    <Space direction="vertical">
      <Search placeholder="input search text" onSearch={onSearch} style={{ width: 200 }} />
    </Space>

  )
}