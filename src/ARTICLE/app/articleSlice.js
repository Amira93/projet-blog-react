import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";



const articleSlice = createSlice({
    name : 'articles',
    initialState: {
        list: [],
    },

    reducers:{
        setArticle(state, {payload}){
            state.list = payload;
        },

        addArticle(state, {payload}){
            state.list.push(payload);
        }
    }
});

export const {addArticle, setArticle} = articleSlice.actions;

export default articleSlice.reducer;

export const fetchArticle = () => async (dispatch) =>{
    try {
        const response = await axios.get(' https://projet-blog-api.herokuapp.com/api/article');
        dispatch(setArticle(response.data));
              
    } catch (error) {
        console.log(error)
        
    }
}

export const Addarticle = (article) => async (dispatch) =>{
    try {
        await axios.post(' https://projet-blog-api.herokuapp.com/api/article', article);
        
               
    } catch (error) {
        console.log(error)
        
    }

}

export const deleteArticles = (id) => async(dispatch) => {
    try {
        await axios.delete(' https://projet-blog-api.herokuapp.com/api/article/' + id)
       
    } catch (error) {
        console.log(error)
    }
}

           
        
       

// export const Searcharticle = (article) => async (dispatch) =>{
//     try {
//         const response = await axios.get('http://localhost:8000/api/article?searcf' +article);
//         dispatch(setArticle(response.data));
//     } catch (error) {
//         console.log(error)
        
//     }
// }