
import { Layout } from "antd";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { Navigateur } from "./USER/components/Navigateur";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import './App.css';
import { loginWithToken } from "./USER/stores/auth-slice";
import { Poster } from "./pages/Poster";
import { FetchOnePost } from "./pages/FetchOnePost";

const { Header, Content } = Layout;

function App() {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);


  return (
    <Layout  >
      <Header style={{background: '#a0d911'}} >
        <Navigateur />
      </Header>
      <Content>
        <Switch>
          <Route path="/register">
            <Register />
          </Route>

          <Route path="/login">
            <Login />
          </Route>  

          <Route path="/poster">
            <Poster />
          </Route>

          <Route path="/" exact>
            <Home />
            </Route>
          <Route path="/article/:id" children ={<FetchOnePost/>} >
          </Route>

        </Switch>
      </Content>
      {/* <Footer>
        <p>Copyleft no one</p>
      </Footer> */}
    </Layout>
  );
}

export default App;
