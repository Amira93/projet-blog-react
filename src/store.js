import { configureStore } from "@reduxjs/toolkit";
import articleSlice from "./ARTICLE/app/articleSlice";
import authSlice from "./USER/stores/auth-slice";
import userSlice from "./USER/stores/user-slice";


export const store = configureStore({
    reducer: {
        auth: authSlice,
        users: userSlice,
        articles: articleSlice
    }
})