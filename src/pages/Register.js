import { Card, Divider } from "antd";
import { UserFormulaire } from "../USER/components/UserFormulaire";

export function Register(){
    return(
        <Card >
            <Divider>
            <h1 style={{fontSize: "40px", fontFamily: "serif", color: "gray" }}>S'inscrire</h1>
            </Divider>
            <UserFormulaire />
        </Card>
    )
}