import { Divider } from "antd";
import { AddArticle } from "../ARTICLE/components/AddArticle";




export function Poster() {
    return (
        <div>
            <Divider>
            <h1 style={{fontSize: "40px", fontFamily: "serif", color: "gray"}}>Ajouter un article</h1>
            </Divider>
            <AddArticle />
        </div>
    )
}