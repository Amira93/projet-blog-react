import { Card, Divider } from "antd";
import { LoginForm } from "../USER/components/LoginForm";


export function Login(){
    return(
        <Card >
            <Divider>
            <h1 style={{fontSize: "40px", fontFamily: "serif", color: "gray"}}>Connecter</h1>
            </Divider>
            <LoginForm/>
        </Card>
    )
}