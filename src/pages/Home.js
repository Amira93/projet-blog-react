import { Card, Divider } from "antd"
import { ArticleList } from "../ARTICLE/components/ArticleList";
// import { SearchArticle } from "../ARTICLE/components/SearchArticle";

export function Home() {

    return (
        <Card style={{ background: "black" }} >
            <Divider>
            <h1 style={{ color: "#a0d911", fontSize: 50, marginBottom:0 }}>Blog</h1>
            </Divider>
            <ArticleList />
        </Card>
    )
}