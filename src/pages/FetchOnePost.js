import { Card} from "antd";
import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";



export function FetchOnePost() {

    const [articles, setArticle] = useState(null);
    const { id } = useParams();
   
        async function fetchOne() {
            const response = await axios.get(' https://projet-blog-api.herokuapp.com/api/article/' + id);
            setArticle(response.data[0]);
         }
       
        useEffect(() => {
            fetchOne();
        });

        return (
            <Card >
                {articles &&
               
                     <Card  style={{ width: 300, fontSize: 15, color: "white", background: "black"}}>Title: { articles.contenu}
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Molestias, voluptatum modi ullam sit nemo quaerat unde corrupti quia sequi, illum voluptatem quas odit saepe labore cumque temporibus explicabo ab quisquam!{articles.contenu}</p>
                     </Card>
                    
                
                }


            </Card>
        )
    }